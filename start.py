#Libraries
import RPi.GPIO as GPIO
import time
import board
import busio
import adafruit_vl53l0x

#GPIO Mode (BOARD / BCM)
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
 
#set GPIO Pins
GPIO_TRIGGER1 = 23
GPIO_ECHO1 = 24

GPIO_TRIGGER2 = 22
GPIO_ECHO2 = 27

SERVO = 17

# Initialize variables
ledRed = 16
ledYellow = 20
ledGreen = 21

# Set IO state
GPIO.setup(ledRed, GPIO.OUT)
GPIO.setup(ledYellow, GPIO.OUT)
GPIO.setup(ledGreen, GPIO.OUT)

# Initialize I2C bus and sensor.
i2c = busio.I2C(board.SCL, board.SDA)
vl53 = adafruit_vl53l0x.VL53L0X(i2c)
 
#set GPIO direction (IN / OUT)
GPIO.setup(GPIO_TRIGGER1, GPIO.OUT)
GPIO.setup(GPIO_ECHO1, GPIO.IN)

GPIO.setup(GPIO_TRIGGER2, GPIO.OUT)
GPIO.setup(GPIO_ECHO2, GPIO.IN)

GPIO.setup(SERVO,GPIO.OUT)

servo1 = GPIO.PWM(SERVO,50)
servo1.start(0)


def distance1():
    # set Trigger to HIGH
    GPIO.output(GPIO_TRIGGER1, True)
 
    # set Trigger after 0.01ms to LOW
    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER1, False)
 
    StartTime = time.time()
    StopTime = time.time()
 
    # save StartTime
    while GPIO.input(GPIO_ECHO1) == 0:
        StartTime = time.time()
 
    # save time of arrival
    while GPIO.input(GPIO_ECHO1) == 1:
        StopTime = time.time()
 
    # time difference between start and arrival
    TimeElapsed = StopTime - StartTime
    # multiply with the sonic speed (34300 cm/s)
    # and divide by 2, because there and back
    distance1 = (TimeElapsed * 34300) / 2
 
    return distance1


def distance2():
    # set Trigger to HIGH
    GPIO.output(GPIO_TRIGGER2, True)
 
    # set Trigger after 0.01ms to LOW
    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER2, False)
 
    StartTime = time.time()
    StopTime = time.time()
 
    # save StartTime
    while GPIO.input(GPIO_ECHO2) == 0:
        StartTime = time.time()
 
    # save time of arrival
    while GPIO.input(GPIO_ECHO2) == 1:
        StopTime = time.time()
 
    # time difference between start and arrival
    TimeElapsed = StopTime - StartTime
    # multiply with the sonic speed (34300 cm/s)
    # and divide by 2, because there and back
    distance2 = (TimeElapsed * 34300) / 2
 
    return distance2

 
if __name__ == '__main__':
    try:
        while True:
            dist1 = distance1()
            dist2 = distance2()
            print ("Measured Distance of Right = %df cm" % dist1)
            print ("Measured Distance of Left = %df cm" % dist2)
            time.sleep(1)
            if dist1 < 20:
                deg = 2+(dist1/4)
                servo1.ChangeDutyCycle(deg)
                #Reaction of servo motors is inversely proportional to angle
            elif dist2 < 20:
                deg = 12-(dist2/4)
                servo1.ChangeDutyCycle(deg)
            else:
                deg = 7
                servo1.ChangeDutyCycle(deg)
            print("Range: {0}mm".format(vl53.range))
            if vl53.range >= 1000:
                GPIO.output(ledRed, 0)
                GPIO.output(ledYellow, 0)
                GPIO.output(ledGreen, 1)
            elif vl53.range < 1000:
                GPIO.output(ledGreen, 0)
                if vl53.range >= 300:
                    GPIO.output(ledYellow, 1)
                    GPIO.output(ledRed, 0)
                elif vl53.range < 300:
                    GPIO.output(ledYellow, 0)
                    GPIO.output(ledRed, 1)
 
        # Reset by pressing CTRL + C
    except KeyboardInterrupt:
        print("Measurement stopped by User")
        servo1.stop()
        GPIO.cleanup()

 

